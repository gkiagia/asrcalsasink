#include "audio.h"
#include <gst/base/base.h>

GST_DEBUG_CATEGORY_STATIC (asrcdebug);
#define GST_CAT_DEFAULT asrcdebug

G_DECLARE_FINAL_TYPE (GstAsrc, gst_asrc, GST, ASRC, GstBaseTransform)

struct _GstAsrc
{
  GstBaseTransform parent;

  AsrcAudioInfo *info;

  GstClockTime last_pts;
  gint actual_frames;

  AsrcAudioResampler *resampler;
};

#define gst_asrc_parent_class parent_class
G_DEFINE_TYPE (GstAsrc, gst_asrc, GST_TYPE_BASE_TRANSFORM)

#define SUPPORTED_FORMATS "{ " \
  ASRC_AUDIO_NE(S16) ", " \
  ASRC_AUDIO_NE(S32) ", " \
  ASRC_AUDIO_NE(F32) ", " \
  ASRC_AUDIO_NE(F64) " }"

static GstStaticPadTemplate sink_template = GST_STATIC_PAD_TEMPLATE (
  "sink",
  GST_PAD_SINK,
  GST_PAD_ALWAYS,
  GST_STATIC_CAPS (ASRC_AUDIO_CAPS_MAKE (SUPPORTED_FORMATS))
);

static GstStaticPadTemplate src_template = GST_STATIC_PAD_TEMPLATE (
  "src",
  GST_PAD_SRC,
  GST_PAD_ALWAYS,
  GST_STATIC_CAPS (ASRC_AUDIO_CAPS_MAKE (SUPPORTED_FORMATS))
);

static void
gst_asrc_init (GstAsrc *self)
{
}

static gboolean
gst_asrc_set_caps (GstBaseTransform * filter, GstCaps *incaps, GstCaps *outcaps)
{
  GstAsrc *self = GST_ASRC (filter);
  GstStructure *options;
  AsrcAudioInfo info;

  if (!asrc_audio_info_from_caps (&info, incaps))
    return FALSE;

  self->last_pts = GST_CLOCK_TIME_NONE;

  g_clear_pointer (&self->info, asrc_audio_info_free);
  self->info = asrc_audio_info_copy (&info);

  AsrcAudioResamplerFlags flags = ASRC_AUDIO_RESAMPLER_FLAG_VARIABLE_RATE;
  // if (ASRC_AUDIO_INFO_LAYOUT (info) == ASRC_AUDIO_LAYOUT_NON_INTERLEAVED) {
  //   flags |= ASRC_AUDIO_RESAMPLER_FLAG_NON_INTERLEAVED_IN;
  //   flags |= ASRC_AUDIO_RESAMPLER_FLAG_NON_INTERLEAVED_OUT;
  // }

  options = gst_structure_new_empty ("GstAudioResampler.options");
  asrc_audio_resampler_options_set_quality (
      ASRC_AUDIO_RESAMPLER_METHOD_KAISER,
      ASRC_AUDIO_RESAMPLER_QUALITY_DEFAULT,
      ASRC_AUDIO_INFO_RATE (&info),
      ASRC_AUDIO_INFO_RATE (&info),
      options);

  g_clear_pointer (&self->resampler, asrc_audio_resampler_free);
  self->resampler = asrc_audio_resampler_new (
      ASRC_AUDIO_RESAMPLER_METHOD_KAISER,
      flags,
      ASRC_AUDIO_INFO_FORMAT (&info),
      ASRC_AUDIO_INFO_CHANNELS (&info),
      ASRC_AUDIO_INFO_RATE (&info),
      ASRC_AUDIO_INFO_RATE (&info),
      options);

  gst_structure_free (options);

  return TRUE;
}

static gboolean
gst_asrc_stop (GstBaseTransform *trans)
{
  GstAsrc *self = GST_ASRC (trans);
  g_clear_pointer (&self->info, asrc_audio_info_free);
  g_clear_pointer (&self->resampler, asrc_audio_resampler_free);
  return TRUE;
}

static gboolean
gst_asrc_transform_size (GstBaseTransform *trans,
    GstPadDirection direction,
    GstCaps *caps, gsize size,
    GstCaps *othercaps, gsize *othersize)
{
  GstAsrc *self = GST_ASRC (trans);

  gsize frames = size / ASRC_AUDIO_INFO_BPF (self->info);
  gsize res_frames;

  if (direction == GST_PAD_SINK)
    res_frames = asrc_audio_resampler_get_out_frames (self->resampler, frames);
  else
    res_frames = asrc_audio_resampler_get_in_frames (self->resampler, frames);

  GST_DEBUG_OBJECT (self, "in: %" G_GSIZE_FORMAT ", out: %" G_GSIZE_FORMAT,
      frames, res_frames);

  *othersize = res_frames * ASRC_AUDIO_INFO_BPF (self->info);
  return TRUE;
}

static GstFlowReturn
do_resample (GstAsrc *self, GstBuffer *inbuf, GstBuffer *outbuf)
{
  GstMapInfo inmap, outmap;
  gpointer inframes[1], outframes[1];

  gsize n_in_frames =
      gst_buffer_get_size (inbuf) / ASRC_AUDIO_INFO_BPF (self->info);
  gsize n_out_frames =
      asrc_audio_resampler_get_out_frames (self->resampler, n_in_frames);

  gst_buffer_set_size (outbuf, n_out_frames * ASRC_AUDIO_INFO_BPF (self->info));
  GST_BUFFER_DURATION (outbuf) =
      GST_FRAMES_TO_CLOCK_TIME (n_out_frames, ASRC_AUDIO_INFO_RATE (self->info));

  if (!gst_buffer_map (inbuf, &inmap, GST_MAP_READ))
    return GST_FLOW_ERROR;

  if (!gst_buffer_map (outbuf, &outmap, GST_MAP_READWRITE)) {
    gst_buffer_unmap (inbuf, &inmap);
    return GST_FLOW_ERROR;
  }

  inframes[0] = inmap.data;
  outframes[0] = outmap.data;
  asrc_audio_resampler_resample (self->resampler,
      inframes, n_in_frames, outframes, n_out_frames);

  gst_buffer_unmap (inbuf, &inmap);
  gst_buffer_unmap (outbuf, &outmap);

  return GST_FLOW_OK;
}

static GstFlowReturn
gst_asrc_transform (GstBaseTransform *trans, GstBuffer *inbuf, GstBuffer *outbuf)
{
  GstAsrc *self = GST_ASRC (trans);
  GstFlowReturn ret = GST_FLOW_OK;
  gdouble corr = 1.0;

  if (self->last_pts != GST_CLOCK_TIME_NONE && !GST_BUFFER_IS_DISCONT (inbuf)) {
    GstClockTime calibrated_dur;
    gint calibrated_frames;

    calibrated_dur = GST_BUFFER_PTS (inbuf) - self->last_pts;
    calibrated_frames = GST_CLOCK_TIME_TO_FRAMES (calibrated_dur,
        ASRC_AUDIO_INFO_RATE (self->info));

    corr = (gdouble) self->actual_frames / (gdouble) calibrated_frames;

    GST_INFO_OBJECT (self, "corr: %lf, calibrated: %d, exp: %d",
        corr, calibrated_frames, self->actual_frames);
  }

  ret = do_resample (self, inbuf, outbuf);

  asrc_audio_resampler_update (self->resampler,
      ASRC_AUDIO_INFO_RATE (self->info) * corr,
      ASRC_AUDIO_INFO_RATE (self->info),
      NULL);

  self->last_pts = GST_BUFFER_PTS (inbuf);
  self->actual_frames = GST_CLOCK_TIME_TO_FRAMES (
      GST_BUFFER_DURATION (inbuf),
      ASRC_AUDIO_INFO_RATE (self->info));
  return ret;
}

static void
gst_asrc_class_init (GstAsrcClass *class)
{
  GstElementClass *eclass = GST_ELEMENT_CLASS (class);
  GstBaseTransformClass *btclass = GST_BASE_TRANSFORM_CLASS (class);

  gst_element_class_set_static_metadata (eclass,
      "GstAsrc",
      "Filter/Effect/Audio",
      "Adaptive Stream Rate Control",
      "George Kiagiadakis <george.kiagiadakis@collabora.com>");

  gst_element_class_add_static_pad_template (eclass, &sink_template);
  gst_element_class_add_static_pad_template (eclass, &src_template);

  btclass->set_caps = gst_asrc_set_caps;
  btclass->stop = gst_asrc_stop;
  btclass->transform_size = gst_asrc_transform_size;
  btclass->transform = gst_asrc_transform;

  GST_DEBUG_CATEGORY_INIT (asrcdebug, "asrc", 0, "Asrc element debug");
}

static gboolean
plugin_init (GstPlugin *plugin)
{
  if (!gst_element_register (plugin, "asrc", GST_RANK_NONE,
          gst_asrc_get_type ()))
    return FALSE;

  return TRUE;
}

#define PACKAGE "asrcalsaplugins"

GST_PLUGIN_DEFINE (GST_VERSION_MAJOR,
    GST_VERSION_MINOR,
    asrc,
    "ASRC plugin library",
    plugin_init, "1.8", "LGPL", "ASRC Plugins", "Collabora")
