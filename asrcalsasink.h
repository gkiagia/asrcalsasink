/* GStreamer
 * Copyright (C)  2005 Wim Taymans <wim@fluendo.com>
 *
 * asrcalsasink.h: 
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */


#ifndef __ASRC_ALSASINK_H__
#define __ASRC_ALSASINK_H__

#include <gst/gst.h>
#include <alsa/asoundlib.h>

#include "audio.h"

G_BEGIN_DECLS

#define ASRC_TYPE_ALSA_SINK            (asrc_alsasink_get_type())
#define ASRC_ALSA_SINK(obj)            (G_TYPE_CHECK_INSTANCE_CAST((obj),ASRC_TYPE_ALSA_SINK,AsrcAlsaSink))
#define ASRC_ALSA_SINK_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST((klass),ASRC_TYPE_ALSA_SINK,AsrcAlsaSinkClass))
#define GST_IS_ALSA_SINK(obj)         (G_TYPE_CHECK_INSTANCE_TYPE((obj),ASRC_TYPE_ALSA_SINK))
#define GST_IS_ALSA_SINK_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE((klass),ASRC_TYPE_ALSA_SINK))
#define ASRC_ALSA_SINK_CAST(obj)       ((AsrcAlsaSink *) (obj))

typedef struct _AsrcAlsaSink AsrcAlsaSink;
typedef struct _AsrcAlsaSinkClass AsrcAlsaSinkClass;

#define ASRC_ALSA_SINK_GET_LOCK(obj)	(&ASRC_ALSA_SINK_CAST (obj)->alsa_lock)
#define ASRC_ALSA_SINK_LOCK(obj)	    (g_mutex_lock (ASRC_ALSA_SINK_GET_LOCK (obj)))
#define ASRC_ALSA_SINK_UNLOCK(obj)   (g_mutex_unlock (ASRC_ALSA_SINK_GET_LOCK (obj)))

#define GST_DELAY_SINK_GET_LOCK(obj)	(&ASRC_ALSA_SINK_CAST (obj)->delay_lock)
#define GST_DELAY_SINK_LOCK(obj)	        (g_mutex_lock (GST_DELAY_SINK_GET_LOCK (obj)))
#define GST_DELAY_SINK_UNLOCK(obj)	(g_mutex_unlock (GST_DELAY_SINK_GET_LOCK (obj)))

/**
 * AsrcAlsaSink:
 *
 * Opaque data structure
 */
struct _AsrcAlsaSink {
  AsrcAudioSink    sink;

  gchar                 *device;

  snd_pcm_t             *handle;
  snd_pcm_hw_params_t   *hwparams;
  snd_pcm_sw_params_t   *swparams;

  snd_pcm_access_t access;
  snd_pcm_format_t format;
  guint rate;
  guint channels;
  gint bpf;
  gboolean iec958;
  gboolean need_swap;

  guint buffer_time;
  guint period_time;
  snd_pcm_uframes_t buffer_size;
  snd_pcm_uframes_t period_size;

  GstCaps *cached_caps;

  GMutex alsa_lock;
  GMutex delay_lock;
};

struct _AsrcAlsaSinkClass {
  AsrcAudioSinkClass parent_class;
};

GType asrc_alsasink_get_type(void);

G_END_DECLS

#endif /* __ASRC_ALSASINK_H__ */
