/* GStreamer
 * Copyright (C) 1999,2000 Erik Walthinsen <omega@cse.ogi.edu>
 *                    2005 Wim Taymans <wim@fluendo.com>
 *
 * asrcaudioclock.h: Clock for use by audio plugins
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#ifndef __ASRC_AUDIO_AUDIO_H__
#include "audio.h"
#endif

#ifndef __ASRC_AUDIO_CLOCK_H__
#define __ASRC_AUDIO_CLOCK_H__

#include <gst/gst.h>
#include <gst/gstsystemclock.h>

G_BEGIN_DECLS

#define ASRC_TYPE_AUDIO_CLOCK \
  (asrc_audio_clock_get_type())
#define ASRC_AUDIO_CLOCK(obj) \
  (G_TYPE_CHECK_INSTANCE_CAST((obj),ASRC_TYPE_AUDIO_CLOCK,AsrcAudioClock))
#define ASRC_AUDIO_CLOCK_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_CAST((klass),ASRC_TYPE_AUDIO_CLOCK,AsrcAudioClockClass))
#define GST_IS_AUDIO_CLOCK(obj) \
  (G_TYPE_CHECK_INSTANCE_TYPE((obj),ASRC_TYPE_AUDIO_CLOCK))
#define GST_IS_AUDIO_CLOCK_CLASS(klass) \
  (G_TYPE_CHECK_CLASS_TYPE((klass),ASRC_TYPE_AUDIO_CLOCK))
#define ASRC_AUDIO_CLOCK_CAST(obj) \
  ((AsrcAudioClock*)(obj))

typedef struct _AsrcAudioClock AsrcAudioClock;
typedef struct _AsrcAudioClockClass AsrcAudioClockClass;

/**
 * AsrcAudioClockGetTimeFunc:
 * @clock: the #AsrcAudioClock
 * @user_data: user data
 *
 * This function will be called whenever the current clock time needs to be
 * calculated. If this function returns #GST_CLOCK_TIME_NONE, the last reported
 * time will be returned by the clock.
 *
 * Returns: the current time or #GST_CLOCK_TIME_NONE if the previous time should
 * be used.
 */
typedef GstClockTime (*AsrcAudioClockGetTimeFunc) (GstClock *clock, gpointer user_data);

/**
 * AsrcAudioClock:
 *
 * Opaque #AsrcAudioClock.
 */
struct _AsrcAudioClock {
  GstSystemClock clock;

  /*< protected >*/
  AsrcAudioClockGetTimeFunc func;
  gpointer                 user_data;
  GDestroyNotify           destroy_notify;

  /*< private >*/
  GstClockTime             last_time;
  GstClockTimeDiff         time_offset;

  gpointer _gst_reserved[GST_PADDING];
};

struct _AsrcAudioClockClass {
  GstSystemClockClass parent_class;

  /*< private >*/
  gpointer _gst_reserved[GST_PADDING];
};

GType           asrc_audio_clock_get_type        (void);
GstClock*       asrc_audio_clock_new             (const gchar *name, AsrcAudioClockGetTimeFunc func,
                                                 gpointer user_data, GDestroyNotify destroy_notify);
void            asrc_audio_clock_reset           (AsrcAudioClock *clock, GstClockTime time);

GstClockTime    asrc_audio_clock_get_time        (GstClock * clock);
GstClockTime    asrc_audio_clock_adjust          (GstClock * clock, GstClockTime time);

void            asrc_audio_clock_invalidate      (GstClock * clock);

#ifdef G_DEFINE_AUTOPTR_CLEANUP_FUNC
G_DEFINE_AUTOPTR_CLEANUP_FUNC(AsrcAudioClock, gst_object_unref)
#endif

G_END_DECLS

#endif /* __ASRC_AUDIO_CLOCK_H__ */
