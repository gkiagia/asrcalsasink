/* GStreamer
 * Copyright (C) 1999,2000 Erik Walthinsen <omega@cse.ogi.edu>
 *                    2005 Wim Taymans <wim@fluendo.com>
 *
 * asrcaudioringbuffer.h:
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#ifndef __ASRC_AUDIO_AUDIO_H__
#include "audio.h"
#endif

#ifndef __ASRC_AUDIO_RING_BUFFER_H__
#define __ASRC_AUDIO_RING_BUFFER_H__

G_BEGIN_DECLS

#define ASRC_TYPE_AUDIO_RING_BUFFER             (asrc_audio_ring_buffer_get_type())
#define ASRC_AUDIO_RING_BUFFER(obj)             (G_TYPE_CHECK_INSTANCE_CAST((obj),ASRC_TYPE_AUDIO_RING_BUFFER,AsrcAudioRingBuffer))
#define ASRC_AUDIO_RING_BUFFER_CLASS(klass)     (G_TYPE_CHECK_CLASS_CAST((klass),ASRC_TYPE_AUDIO_RING_BUFFER,AsrcAudioRingBufferClass))
#define ASRC_AUDIO_RING_BUFFER_GET_CLASS(obj)   (G_TYPE_INSTANCE_GET_CLASS ((obj), ASRC_TYPE_AUDIO_RING_BUFFER, AsrcAudioRingBufferClass))
#define ASRC_AUDIO_RING_BUFFER_CAST(obj)        ((AsrcAudioRingBuffer *)obj)
#define GST_IS_AUDIO_RING_BUFFER(obj)          (G_TYPE_CHECK_INSTANCE_TYPE((obj),ASRC_TYPE_AUDIO_RING_BUFFER))
#define GST_IS_AUDIO_RING_BUFFER_CLASS(klass)  (G_TYPE_CHECK_CLASS_TYPE((klass),ASRC_TYPE_AUDIO_RING_BUFFER))

typedef struct _AsrcAudioRingBuffer AsrcAudioRingBuffer;
typedef struct _AsrcAudioRingBufferClass AsrcAudioRingBufferClass;
typedef struct _AsrcAudioRingBufferSpec AsrcAudioRingBufferSpec;

/**
 * AsrcAudioRingBufferCallback:
 * @rbuf: a #AsrcAudioRingBuffer
 * @data: (array length=len): target to fill
 * @len: amount to fill
 * @user_data: user data
 *
 * This function is set with asrc_audio_ring_buffer_set_callback() and is
 * called to fill the memory at @data with @len bytes of samples.
 */
typedef void (*AsrcAudioRingBufferCallback) (AsrcAudioRingBuffer *rbuf, guint8* data, guint len, gpointer user_data);

/**
 * AsrcAudioRingBufferState:
 * @ASRC_AUDIO_RING_BUFFER_STATE_STOPPED: The ringbuffer is stopped
 * @ASRC_AUDIO_RING_BUFFER_STATE_PAUSED: The ringbuffer is paused
 * @ASRC_AUDIO_RING_BUFFER_STATE_STARTED: The ringbuffer is started
 * @ASRC_AUDIO_RING_BUFFER_STATE_ERROR: The ringbuffer has encountered an
 *     error after it has been started, e.g. because the device was
 *     disconnected (Since 1.2)
 *
 * The state of the ringbuffer.
 */
typedef enum {
  ASRC_AUDIO_RING_BUFFER_STATE_STOPPED,
  ASRC_AUDIO_RING_BUFFER_STATE_PAUSED,
  ASRC_AUDIO_RING_BUFFER_STATE_STARTED,
  ASRC_AUDIO_RING_BUFFER_STATE_ERROR
} AsrcAudioRingBufferState;

/**
 * AsrcAudioRingBufferFormatType:
 * @ASRC_AUDIO_RING_BUFFER_FORMAT_TYPE_RAW: samples in linear or float
 * @ASRC_AUDIO_RING_BUFFER_FORMAT_TYPE_MU_LAW: samples in mulaw
 * @ASRC_AUDIO_RING_BUFFER_FORMAT_TYPE_A_LAW: samples in alaw
 * @ASRC_AUDIO_RING_BUFFER_FORMAT_TYPE_IMA_ADPCM: samples in ima adpcm
 * @ASRC_AUDIO_RING_BUFFER_FORMAT_TYPE_MPEG: samples in mpeg audio (but not AAC) format
 * @ASRC_AUDIO_RING_BUFFER_FORMAT_TYPE_GSM: samples in gsm format
 * @ASRC_AUDIO_RING_BUFFER_FORMAT_TYPE_IEC958: samples in IEC958 frames (e.g. AC3)
 * @ASRC_AUDIO_RING_BUFFER_FORMAT_TYPE_AC3: samples in AC3 format
 * @ASRC_AUDIO_RING_BUFFER_FORMAT_TYPE_EAC3: samples in EAC3 format
 * @ASRC_AUDIO_RING_BUFFER_FORMAT_TYPE_DTS: samples in DTS format
 * @ASRC_AUDIO_RING_BUFFER_FORMAT_TYPE_MPEG2_AAC: samples in MPEG-2 AAC format
 * @ASRC_AUDIO_RING_BUFFER_FORMAT_TYPE_MPEG4_AAC: samples in MPEG-4 AAC format
 *
 * The format of the samples in the ringbuffer.
 */
typedef enum
{
  ASRC_AUDIO_RING_BUFFER_FORMAT_TYPE_RAW,
  ASRC_AUDIO_RING_BUFFER_FORMAT_TYPE_MU_LAW,
  ASRC_AUDIO_RING_BUFFER_FORMAT_TYPE_A_LAW,
  ASRC_AUDIO_RING_BUFFER_FORMAT_TYPE_IMA_ADPCM,
  ASRC_AUDIO_RING_BUFFER_FORMAT_TYPE_MPEG,
  ASRC_AUDIO_RING_BUFFER_FORMAT_TYPE_GSM,
  ASRC_AUDIO_RING_BUFFER_FORMAT_TYPE_IEC958,
  ASRC_AUDIO_RING_BUFFER_FORMAT_TYPE_AC3,
  ASRC_AUDIO_RING_BUFFER_FORMAT_TYPE_EAC3,
  ASRC_AUDIO_RING_BUFFER_FORMAT_TYPE_DTS,
  ASRC_AUDIO_RING_BUFFER_FORMAT_TYPE_MPEG2_AAC,
  ASRC_AUDIO_RING_BUFFER_FORMAT_TYPE_MPEG4_AAC
} AsrcAudioRingBufferFormatType;

/**
 * AsrcAudioRingBufferSpec:
 * @caps: The caps that generated the Spec.
 * @type: the sample type
 * @info: the #AsrcAudioInfo
 * @latency_time: the latency in microseconds
 * @buffer_time: the total buffer size in microseconds
 * @segsize: the size of one segment in bytes
 * @segtotal: the total number of segments
 * @seglatency: number of segments queued in the lower level device,
 *  defaults to segtotal
 *
 * The structure containing the format specification of the ringbuffer.
 */
struct _AsrcAudioRingBufferSpec
{
  /*< public >*/
  /* in */
  GstCaps  *caps;               /* the caps of the buffer */

  /* in/out */
  AsrcAudioRingBufferFormatType  type;
  AsrcAudioInfo                  info;


  guint64  latency_time;        /* the required/actual latency time, this is the
				 * actual the size of one segment and the
				 * minimum possible latency we can achieve. */
  guint64  buffer_time;         /* the required/actual time of the buffer, this is
				 * the total size of the buffer and maximum
				 * latency we can compensate for. */
  gint     segsize;             /* size of one buffer segment in bytes, this value
				 * should be chosen to match latency_time as
				 * well as possible. */
  gint     segtotal;            /* total number of segments, this value is the
				 * number of segments of @segsize and should be
				 * chosen so that it matches buffer_time as
				 * close as possible. */
  /* ABI added 0.10.20 */
  gint     seglatency;          /* number of segments queued in the lower
				 * level device, defaults to segtotal. */

  /*< private >*/
  gpointer _gst_reserved[GST_PADDING];
};

#define ASRC_AUDIO_RING_BUFFER_GET_COND(buf) (&(((AsrcAudioRingBuffer *)buf)->cond))
#define ASRC_AUDIO_RING_BUFFER_WAIT(buf)     (g_cond_wait (ASRC_AUDIO_RING_BUFFER_GET_COND (buf), GST_OBJECT_GET_LOCK (buf)))
#define ASRC_AUDIO_RING_BUFFER_SIGNAL(buf)   (g_cond_signal (ASRC_AUDIO_RING_BUFFER_GET_COND (buf)))
#define ASRC_AUDIO_RING_BUFFER_BROADCAST(buf)(g_cond_broadcast (ASRC_AUDIO_RING_BUFFER_GET_COND (buf)))

/**
 * AsrcAudioRingBuffer:
 * @cond: used to signal start/stop/pause/resume actions
 * @open: boolean indicating that the ringbuffer is open
 * @acquired: boolean indicating that the ringbuffer is acquired
 * @memory: data in the ringbuffer
 * @size: size of data in the ringbuffer
 * @spec: format and layout of the ringbuffer data
 * @samples_per_seg: number of samples in one segment
 * @empty_seg: pointer to memory holding one segment of silence samples
 * @state: state of the buffer
 * @segdone: readpointer in the ringbuffer
 * @segbase: segment corresponding to segment 0 (unused)
 * @waiting: is a reader or writer waiting for a free segment
 *
 * The ringbuffer base class structure.
 */
struct _AsrcAudioRingBuffer {
  GstObject                   object;

  /*< public >*/ /* with LOCK */
  GCond                      cond;
  gboolean                    open;
  gboolean                    acquired;
  guint8                     *memory;
  gsize                       size;
  GstClockTime               *timestamps;
  AsrcAudioRingBufferSpec      spec;
  gint                        samples_per_seg;
  guint8                     *empty_seg;

  /*< public >*/ /* ATOMIC */
  gint                        state;
  gint                        segdone;
  gint                        segbase;
  gint                        waiting;

  /*< private >*/
  AsrcAudioRingBufferCallback  callback;
  gpointer                    cb_data;

  gboolean                    need_reorder;
  /* gst[channel_reorder_map[i]] = device[i] */
  gint                        channel_reorder_map[64];

  gboolean                    flushing;
  /* ATOMIC */
  gint                        may_start;
  gboolean                    active;

  /*< private >*/
  gpointer _gst_reserved[GST_PADDING];
};

/**
 * AsrcAudioRingBufferClass:
 * @parent_class: parent class
 * @open_device:  open the device, don't set any params or allocate anything
 * @acquire: allocate the resources for the ringbuffer using the given spec
 * @release: free resources of the ringbuffer
 * @close_device: close the device
 * @start: start processing of samples
 * @pause: pause processing of samples
 * @resume: resume processing of samples after pause
 * @stop: stop processing of samples
 * @delay: get number of frames queued in device
 * @activate: activate the thread that starts pulling and monitoring the
 * consumed segments in the device.
 * @commit: write samples into the ringbuffer
 * @clear_all: clear the entire ringbuffer.
 *
 * The vmethods that subclasses can override to implement the ringbuffer.
 */
struct _AsrcAudioRingBufferClass {
  GstObjectClass parent_class;

  /*< public >*/
  gboolean     (*open_device)  (AsrcAudioRingBuffer *buf);
  gboolean     (*acquire)      (AsrcAudioRingBuffer *buf, AsrcAudioRingBufferSpec *spec);
  gboolean     (*release)      (AsrcAudioRingBuffer *buf);
  gboolean     (*close_device) (AsrcAudioRingBuffer *buf);

  gboolean     (*start)        (AsrcAudioRingBuffer *buf);
  gboolean     (*pause)        (AsrcAudioRingBuffer *buf);
  gboolean     (*resume)       (AsrcAudioRingBuffer *buf);
  gboolean     (*stop)         (AsrcAudioRingBuffer *buf);

  guint        (*delay)        (AsrcAudioRingBuffer *buf);

  /* ABI added */
  gboolean     (*activate)     (AsrcAudioRingBuffer *buf, gboolean active);

  guint        (*commit)       (AsrcAudioRingBuffer * buf, guint64 *sample,
                                guint8 * data, gint in_samples,
                                gint out_samples, gint * accum);

  void         (*clear_all)    (AsrcAudioRingBuffer * buf);

  /*< private >*/
  gpointer _gst_reserved[GST_PADDING];
};

GType asrc_audio_ring_buffer_get_type(void);

/* callback stuff */
void            asrc_audio_ring_buffer_set_callback    (AsrcAudioRingBuffer *buf,
                                                       AsrcAudioRingBufferCallback cb,
                                                       gpointer user_data);

gboolean        asrc_audio_ring_buffer_parse_caps      (AsrcAudioRingBufferSpec *spec, GstCaps *caps);
void            asrc_audio_ring_buffer_debug_spec_caps (AsrcAudioRingBufferSpec *spec);
void            asrc_audio_ring_buffer_debug_spec_buff (AsrcAudioRingBufferSpec *spec);

gboolean        asrc_audio_ring_buffer_convert         (AsrcAudioRingBuffer * buf, GstFormat src_fmt,
                                                       gint64 src_val, GstFormat dest_fmt,
                                                       gint64 * dest_val);

/* device state */
gboolean        asrc_audio_ring_buffer_open_device     (AsrcAudioRingBuffer *buf);
gboolean        asrc_audio_ring_buffer_close_device    (AsrcAudioRingBuffer *buf);

gboolean        asrc_audio_ring_buffer_device_is_open  (AsrcAudioRingBuffer *buf);

/* allocate resources */
gboolean        asrc_audio_ring_buffer_acquire         (AsrcAudioRingBuffer *buf, AsrcAudioRingBufferSpec *spec);
gboolean        asrc_audio_ring_buffer_release         (AsrcAudioRingBuffer *buf);

gboolean        asrc_audio_ring_buffer_is_acquired     (AsrcAudioRingBuffer *buf);

/* set the device channel positions */
void            asrc_audio_ring_buffer_set_channel_positions (AsrcAudioRingBuffer *buf, const AsrcAudioChannelPosition *position);

/* activating */
gboolean        asrc_audio_ring_buffer_activate        (AsrcAudioRingBuffer *buf, gboolean active);
gboolean        asrc_audio_ring_buffer_is_active       (AsrcAudioRingBuffer *buf);

/* flushing */
void            asrc_audio_ring_buffer_set_flushing    (AsrcAudioRingBuffer *buf, gboolean flushing);
gboolean        asrc_audio_ring_buffer_is_flushing     (AsrcAudioRingBuffer *buf);

/* playback/pause */
gboolean        asrc_audio_ring_buffer_start           (AsrcAudioRingBuffer *buf);
gboolean        asrc_audio_ring_buffer_pause           (AsrcAudioRingBuffer *buf);
gboolean        asrc_audio_ring_buffer_stop            (AsrcAudioRingBuffer *buf);

/* get status */
guint           asrc_audio_ring_buffer_delay           (AsrcAudioRingBuffer *buf);
guint64         asrc_audio_ring_buffer_samples_done    (AsrcAudioRingBuffer *buf);

void            asrc_audio_ring_buffer_set_sample      (AsrcAudioRingBuffer *buf, guint64 sample);

/* clear all segments */
void            asrc_audio_ring_buffer_clear_all       (AsrcAudioRingBuffer *buf);

/* commit samples */
guint           asrc_audio_ring_buffer_commit          (AsrcAudioRingBuffer * buf, guint64 *sample,
                                                       guint8 * data, gint in_samples,
                                                       gint out_samples, gint * accum);

/* read samples */
guint           asrc_audio_ring_buffer_read            (AsrcAudioRingBuffer *buf, guint64 sample,
                                                       guint8 *data, guint len, GstClockTime *timestamp);

/* Set timestamp on buffer */
void            asrc_audio_ring_buffer_set_timestamp   (AsrcAudioRingBuffer * buf, gint readseg, GstClockTime 
                                                       timestamp);

/* mostly protected */
/* not yet implemented
gboolean        asrc_audio_ring_buffer_prepare_write   (AsrcAudioRingBuffer *buf, gint *segment, guint8 **writeptr, gint *len);
*/
gboolean        asrc_audio_ring_buffer_prepare_read    (AsrcAudioRingBuffer *buf, gint *segment,
                                                       guint8 **readptr, gint *len);
void            asrc_audio_ring_buffer_clear           (AsrcAudioRingBuffer *buf, gint segment);
void            asrc_audio_ring_buffer_advance         (AsrcAudioRingBuffer *buf, guint advance);

void            asrc_audio_ring_buffer_may_start       (AsrcAudioRingBuffer *buf, gboolean allowed);

#ifdef G_DEFINE_AUTOPTR_CLEANUP_FUNC
G_DEFINE_AUTOPTR_CLEANUP_FUNC(AsrcAudioRingBuffer, gst_object_unref)
#endif

G_END_DECLS

#endif /* __ASRC_AUDIO_RING_BUFFER_H__ */
