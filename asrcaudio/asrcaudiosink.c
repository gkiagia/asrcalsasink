/* GStreamer
 * Copyright (C) 1999,2000 Erik Walthinsen <omega@cse.ogi.edu>
 *                    2005 Wim Taymans <wim@fluendo.com>
 *
 * asrcaudiosink.c: simple audio sink base class
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

/**
 * SECTION:asrcaudiosink
 * @short_description: Simple base class for audio sinks
 * @see_also: #AsrcAudioBaseSink, #AsrcAudioRingBuffer, #AsrcAudioSink.
 *
 * This is the most simple base class for audio sinks that only requires
 * subclasses to implement a set of simple functions:
 *
 * <variablelist>
 *   <varlistentry>
 *     <term>open()</term>
 *     <listitem><para>Open the device.</para></listitem>
 *   </varlistentry>
 *   <varlistentry>
 *     <term>prepare()</term>
 *     <listitem><para>Configure the device with the specified format.</para></listitem>
 *   </varlistentry>
 *   <varlistentry>
 *     <term>write()</term>
 *     <listitem><para>Write samples to the device.</para></listitem>
 *   </varlistentry>
 *   <varlistentry>
 *     <term>reset()</term>
 *     <listitem><para>Unblock writes and flush the device.</para></listitem>
 *   </varlistentry>
 *   <varlistentry>
 *     <term>delay()</term>
 *     <listitem><para>Get the number of samples written but not yet played
 *     by the device.</para></listitem>
 *   </varlistentry>
 *   <varlistentry>
 *     <term>unprepare()</term>
 *     <listitem><para>Undo operations done by prepare.</para></listitem>
 *   </varlistentry>
 *   <varlistentry>
 *     <term>close()</term>
 *     <listitem><para>Close the device.</para></listitem>
 *   </varlistentry>
 * </variablelist>
 *
 * All scheduling of samples and timestamps is done in this base class
 * together with #AsrcAudioBaseSink using a default implementation of a
 * #AsrcAudioRingBuffer that uses threads.
 */

#include <string.h>

#include "audio.h"
#include "asrcaudiosink.h"

GST_DEBUG_CATEGORY_STATIC (asrc_audio_sink_debug);
#define GST_CAT_DEFAULT asrc_audio_sink_debug

#define ASRC_TYPE_AUDIO_SINK_RING_BUFFER        \
        (asrc_audio_sink_ring_buffer_get_type())
#define ASRC_AUDIO_SINK_RING_BUFFER(obj)        \
        (G_TYPE_CHECK_INSTANCE_CAST((obj),ASRC_TYPE_AUDIO_SINK_RING_BUFFER,AsrcAudioSinkRingBuffer))
#define ASRC_AUDIO_SINK_RING_BUFFER_CLASS(klass) \
        (G_TYPE_CHECK_CLASS_CAST((klass),ASRC_TYPE_AUDIO_SINK_RING_BUFFER,AsrcAudioSinkRingBufferClass))
#define ASRC_AUDIO_SINK_RING_BUFFER_GET_CLASS(obj) \
        (G_TYPE_INSTANCE_GET_CLASS ((obj), ASRC_TYPE_AUDIO_SINK_RING_BUFFER, AsrcAudioSinkRingBufferClass))
#define ASRC_AUDIO_SINK_RING_BUFFER_CAST(obj)        \
        ((AsrcAudioSinkRingBuffer *)obj)
#define GST_IS_AUDIO_SINK_RING_BUFFER(obj)     \
        (G_TYPE_CHECK_INSTANCE_TYPE((obj),ASRC_TYPE_AUDIO_SINK_RING_BUFFER))
#define GST_IS_AUDIO_SINK_RING_BUFFER_CLASS(klass)\
        (G_TYPE_CHECK_CLASS_TYPE((klass),ASRC_TYPE_AUDIO_SINK_RING_BUFFER))

typedef struct _AsrcAudioSinkRingBuffer AsrcAudioSinkRingBuffer;
typedef struct _AsrcAudioSinkRingBufferClass AsrcAudioSinkRingBufferClass;

#define ASRC_AUDIO_SINK_RING_BUFFER_GET_COND(buf) (&(((AsrcAudioSinkRingBuffer *)buf)->cond))
#define ASRC_AUDIO_SINK_RING_BUFFER_WAIT(buf)     (g_cond_wait (ASRC_AUDIO_SINK_RING_BUFFER_GET_COND (buf), GST_OBJECT_GET_LOCK (buf)))
#define ASRC_AUDIO_SINK_RING_BUFFER_SIGNAL(buf)   (g_cond_signal (ASRC_AUDIO_SINK_RING_BUFFER_GET_COND (buf)))
#define ASRC_AUDIO_SINK_RING_BUFFER_BROADCAST(buf)(g_cond_broadcast (ASRC_AUDIO_SINK_RING_BUFFER_GET_COND (buf)))

struct _AsrcAudioSinkRingBuffer
{
  AsrcAudioRingBuffer object;

  gboolean running;
  gint queuedseg;

  GCond cond;
};

struct _AsrcAudioSinkRingBufferClass
{
  AsrcAudioRingBufferClass parent_class;
};

static void asrc_audio_sink_ring_buffer_class_init (AsrcAudioSinkRingBufferClass *
    klass);
static void asrc_audio_sink_ring_buffer_init (AsrcAudioSinkRingBuffer *
    ringbuffer, AsrcAudioSinkRingBufferClass * klass);
static void asrc_audio_sink_ring_buffer_dispose (GObject * object);
static void asrc_audio_sink_ring_buffer_finalize (GObject * object);

static AsrcAudioRingBufferClass *ring_parent_class = NULL;

static gboolean asrc_audio_sink_ring_buffer_open_device (AsrcAudioRingBuffer *
    buf);
static gboolean asrc_audio_sink_ring_buffer_close_device (AsrcAudioRingBuffer *
    buf);
static gboolean asrc_audio_sink_ring_buffer_acquire (AsrcAudioRingBuffer * buf,
    AsrcAudioRingBufferSpec * spec);
static gboolean asrc_audio_sink_ring_buffer_release (AsrcAudioRingBuffer * buf);
static gboolean asrc_audio_sink_ring_buffer_start (AsrcAudioRingBuffer * buf);
static gboolean asrc_audio_sink_ring_buffer_pause (AsrcAudioRingBuffer * buf);
static gboolean asrc_audio_sink_ring_buffer_stop (AsrcAudioRingBuffer * buf);
static guint asrc_audio_sink_ring_buffer_delay (AsrcAudioRingBuffer * buf);
static gboolean asrc_audio_sink_ring_buffer_activate (AsrcAudioRingBuffer * buf,
    gboolean active);

/* ringbuffer abstract base class */
static GType
asrc_audio_sink_ring_buffer_get_type (void)
{
  static GType ringbuffer_type = 0;

  if (!ringbuffer_type) {
    static const GTypeInfo ringbuffer_info = {
      sizeof (AsrcAudioSinkRingBufferClass),
      NULL,
      NULL,
      (GClassInitFunc) asrc_audio_sink_ring_buffer_class_init,
      NULL,
      NULL,
      sizeof (AsrcAudioSinkRingBuffer),
      0,
      (GInstanceInitFunc) asrc_audio_sink_ring_buffer_init,
      NULL
    };

    ringbuffer_type =
        g_type_register_static (ASRC_TYPE_AUDIO_RING_BUFFER,
        "AsrcAudioSinkRingBuffer", &ringbuffer_info, 0);
  }
  return ringbuffer_type;
}

static void
asrc_audio_sink_ring_buffer_class_init (AsrcAudioSinkRingBufferClass * klass)
{
  GObjectClass *gobject_class;
  AsrcAudioRingBufferClass *gstringbuffer_class;

  gobject_class = (GObjectClass *) klass;
  gstringbuffer_class = (AsrcAudioRingBufferClass *) klass;

  ring_parent_class = g_type_class_peek_parent (klass);

  gobject_class->dispose = asrc_audio_sink_ring_buffer_dispose;
  gobject_class->finalize = asrc_audio_sink_ring_buffer_finalize;

  gstringbuffer_class->open_device =
      GST_DEBUG_FUNCPTR (asrc_audio_sink_ring_buffer_open_device);
  gstringbuffer_class->close_device =
      GST_DEBUG_FUNCPTR (asrc_audio_sink_ring_buffer_close_device);
  gstringbuffer_class->acquire =
      GST_DEBUG_FUNCPTR (asrc_audio_sink_ring_buffer_acquire);
  gstringbuffer_class->release =
      GST_DEBUG_FUNCPTR (asrc_audio_sink_ring_buffer_release);
  gstringbuffer_class->start =
      GST_DEBUG_FUNCPTR (asrc_audio_sink_ring_buffer_start);
  gstringbuffer_class->pause =
      GST_DEBUG_FUNCPTR (asrc_audio_sink_ring_buffer_pause);
  gstringbuffer_class->resume =
      GST_DEBUG_FUNCPTR (asrc_audio_sink_ring_buffer_start);
  gstringbuffer_class->stop =
      GST_DEBUG_FUNCPTR (asrc_audio_sink_ring_buffer_stop);

  gstringbuffer_class->delay =
      GST_DEBUG_FUNCPTR (asrc_audio_sink_ring_buffer_delay);
  gstringbuffer_class->activate =
      GST_DEBUG_FUNCPTR (asrc_audio_sink_ring_buffer_activate);
}

typedef gint (*WriteFunc) (AsrcAudioSink * sink, gpointer data, guint length);

/* this internal thread does nothing else but write samples to the audio device.
 * It will write each segment in the ringbuffer and will update the play
 * pointer.
 * The start/stop methods control the thread.
 */
static void
audioringbuffer_thread_func (AsrcAudioRingBuffer * buf)
{
  AsrcAudioSink *sink;
  AsrcAudioSinkClass *csink;
  AsrcAudioSinkRingBuffer *abuf = ASRC_AUDIO_SINK_RING_BUFFER_CAST (buf);
  WriteFunc writefunc;
  GstMessage *message;
  GValue val = { 0 };

  sink = ASRC_AUDIO_SINK (GST_OBJECT_PARENT (buf));
  csink = ASRC_AUDIO_SINK_GET_CLASS (sink);

  GST_DEBUG_OBJECT (sink, "enter thread");

  GST_OBJECT_LOCK (abuf);
  GST_DEBUG_OBJECT (sink, "signal wait");
  ASRC_AUDIO_SINK_RING_BUFFER_SIGNAL (buf);
  GST_OBJECT_UNLOCK (abuf);

  writefunc = csink->write;
  if (writefunc == NULL)
    goto no_function;

  message = gst_message_new_stream_status (GST_OBJECT_CAST (buf),
      GST_STREAM_STATUS_TYPE_ENTER, GST_ELEMENT_CAST (sink));
  g_value_init (&val, GST_TYPE_G_THREAD);
  g_value_set_boxed (&val, g_thread_self ());
  gst_message_set_stream_status_object (message, &val);
  g_value_unset (&val);
  GST_DEBUG_OBJECT (sink, "posting ENTER stream status");
  gst_element_post_message (GST_ELEMENT_CAST (sink), message);

  while (TRUE) {
    gint left, len;
    guint8 *readptr;
    gint readseg;

    /* buffer must be started */
    if (asrc_audio_ring_buffer_prepare_read (buf, &readseg, &readptr, &len)) {
      gint written;

      left = len;
      do {
        written = writefunc (sink, readptr, left);
        GST_LOG_OBJECT (sink, "transfered %d bytes of %d from segment %d",
            written, left, readseg);
        if (written < 0 || written > left) {
          /* might not be critical, it e.g. happens when aborting playback */
          GST_WARNING_OBJECT (sink,
              "error writing data in %s (reason: %s), skipping segment (left: %d, written: %d)",
              GST_DEBUG_FUNCPTR_NAME (writefunc),
              (errno > 1 ? g_strerror (errno) : "unknown"), left, written);
          break;
        }
        left -= written;
        readptr += written;
      } while (left > 0);

      /* clear written samples */
      asrc_audio_ring_buffer_clear (buf, readseg);

      /* we wrote one segment */
      asrc_audio_ring_buffer_advance (buf, 1);
    } else {
      GST_OBJECT_LOCK (abuf);
      if (!abuf->running)
        goto stop_running;
      if (G_UNLIKELY (g_atomic_int_get (&buf->state) ==
              ASRC_AUDIO_RING_BUFFER_STATE_STARTED)) {
        GST_OBJECT_UNLOCK (abuf);
        continue;
      }
      GST_DEBUG_OBJECT (sink, "signal wait");
      ASRC_AUDIO_SINK_RING_BUFFER_SIGNAL (buf);
      GST_DEBUG_OBJECT (sink, "wait for action");
      ASRC_AUDIO_SINK_RING_BUFFER_WAIT (buf);
      GST_DEBUG_OBJECT (sink, "got signal");
      if (!abuf->running)
        goto stop_running;
      GST_DEBUG_OBJECT (sink, "continue running");
      GST_OBJECT_UNLOCK (abuf);
    }
  }

  /* Will never be reached */
  g_assert_not_reached ();
  return;

  /* ERROR */
no_function:
  {
    GST_DEBUG_OBJECT (sink, "no write function, exit thread");
    return;
  }
stop_running:
  {
    GST_OBJECT_UNLOCK (abuf);
    GST_DEBUG_OBJECT (sink, "stop running, exit thread");
    message = gst_message_new_stream_status (GST_OBJECT_CAST (buf),
        GST_STREAM_STATUS_TYPE_LEAVE, GST_ELEMENT_CAST (sink));
    g_value_init (&val, GST_TYPE_G_THREAD);
    g_value_set_boxed (&val, g_thread_self ());
    gst_message_set_stream_status_object (message, &val);
    g_value_unset (&val);
    GST_DEBUG_OBJECT (sink, "posting LEAVE stream status");
    gst_element_post_message (GST_ELEMENT_CAST (sink), message);
    return;
  }
}

static void
asrc_audio_sink_ring_buffer_init (AsrcAudioSinkRingBuffer * ringbuffer,
    AsrcAudioSinkRingBufferClass * g_class)
{
  ringbuffer->running = FALSE;
  ringbuffer->queuedseg = 0;

  g_cond_init (&ringbuffer->cond);
}

static void
asrc_audio_sink_ring_buffer_dispose (GObject * object)
{
  G_OBJECT_CLASS (ring_parent_class)->dispose (object);
}

static void
asrc_audio_sink_ring_buffer_finalize (GObject * object)
{
  AsrcAudioSinkRingBuffer *ringbuffer = ASRC_AUDIO_SINK_RING_BUFFER_CAST (object);

  g_cond_clear (&ringbuffer->cond);

  G_OBJECT_CLASS (ring_parent_class)->finalize (object);
}

static gboolean
asrc_audio_sink_ring_buffer_open_device (AsrcAudioRingBuffer * buf)
{
  AsrcAudioSink *sink;
  AsrcAudioSinkClass *csink;
  gboolean result = TRUE;

  sink = ASRC_AUDIO_SINK (GST_OBJECT_PARENT (buf));
  csink = ASRC_AUDIO_SINK_GET_CLASS (sink);

  if (csink->open)
    result = csink->open (sink);

  if (!result)
    goto could_not_open;

  return result;

could_not_open:
  {
    GST_DEBUG_OBJECT (sink, "could not open device");
    return FALSE;
  }
}

static gboolean
asrc_audio_sink_ring_buffer_close_device (AsrcAudioRingBuffer * buf)
{
  AsrcAudioSink *sink;
  AsrcAudioSinkClass *csink;
  gboolean result = TRUE;

  sink = ASRC_AUDIO_SINK (GST_OBJECT_PARENT (buf));
  csink = ASRC_AUDIO_SINK_GET_CLASS (sink);

  if (csink->close)
    result = csink->close (sink);

  if (!result)
    goto could_not_close;

  return result;

could_not_close:
  {
    GST_DEBUG_OBJECT (sink, "could not close device");
    return FALSE;
  }
}

static gboolean
asrc_audio_sink_ring_buffer_acquire (AsrcAudioRingBuffer * buf,
    AsrcAudioRingBufferSpec * spec)
{
  AsrcAudioSink *sink;
  AsrcAudioSinkClass *csink;
  gboolean result = FALSE;

  sink = ASRC_AUDIO_SINK (GST_OBJECT_PARENT (buf));
  csink = ASRC_AUDIO_SINK_GET_CLASS (sink);

  if (csink->prepare)
    result = csink->prepare (sink, spec);
  if (!result)
    goto could_not_prepare;

  /* set latency to one more segment as we need some headroom */
  spec->seglatency = spec->segtotal + 1;

  buf->size = spec->segtotal * spec->segsize;

  buf->memory = g_malloc (buf->size);

  if (buf->spec.type == ASRC_AUDIO_RING_BUFFER_FORMAT_TYPE_RAW) {
    asrc_audio_format_fill_silence (buf->spec.info.finfo, buf->memory,
        buf->size);
  } else {
    /* FIXME, non-raw formats get 0 as the empty sample */
    memset (buf->memory, 0, buf->size);
  }


  return TRUE;

  /* ERRORS */
could_not_prepare:
  {
    GST_DEBUG_OBJECT (sink, "could not prepare device");
    return FALSE;
  }
}

static gboolean
asrc_audio_sink_ring_buffer_activate (AsrcAudioRingBuffer * buf, gboolean active)
{
  AsrcAudioSink *sink;
  AsrcAudioSinkRingBuffer *abuf;
  GError *error = NULL;

  sink = ASRC_AUDIO_SINK (GST_OBJECT_PARENT (buf));
  abuf = ASRC_AUDIO_SINK_RING_BUFFER_CAST (buf);

  if (active) {
    abuf->running = TRUE;

    GST_DEBUG_OBJECT (sink, "starting thread");

    sink->thread = g_thread_try_new ("audiosink-ringbuffer",
        (GThreadFunc) audioringbuffer_thread_func, buf, &error);

    if (!sink->thread || error != NULL)
      goto thread_failed;

    GST_DEBUG_OBJECT (sink, "waiting for thread");
    /* the object lock is taken */
    ASRC_AUDIO_SINK_RING_BUFFER_WAIT (buf);
    GST_DEBUG_OBJECT (sink, "thread is started");
  } else {
    abuf->running = FALSE;
    GST_DEBUG_OBJECT (sink, "signal wait");
    ASRC_AUDIO_SINK_RING_BUFFER_SIGNAL (buf);

    GST_OBJECT_UNLOCK (buf);

    /* join the thread */
    g_thread_join (sink->thread);

    GST_OBJECT_LOCK (buf);
  }
  return TRUE;

  /* ERRORS */
thread_failed:
  {
    if (error)
      GST_ERROR_OBJECT (sink, "could not create thread %s", error->message);
    else
      GST_ERROR_OBJECT (sink, "could not create thread for unknown reason");
    g_clear_error (&error);
    return FALSE;
  }
}

/* function is called with LOCK */
static gboolean
asrc_audio_sink_ring_buffer_release (AsrcAudioRingBuffer * buf)
{
  AsrcAudioSink *sink;
  AsrcAudioSinkClass *csink;
  gboolean result = FALSE;

  sink = ASRC_AUDIO_SINK (GST_OBJECT_PARENT (buf));
  csink = ASRC_AUDIO_SINK_GET_CLASS (sink);

  /* free the buffer */
  g_free (buf->memory);
  buf->memory = NULL;

  if (csink->unprepare)
    result = csink->unprepare (sink);

  if (!result)
    goto could_not_unprepare;

  GST_DEBUG_OBJECT (sink, "unprepared");

  return result;

could_not_unprepare:
  {
    GST_DEBUG_OBJECT (sink, "could not unprepare device");
    return FALSE;
  }
}

static gboolean
asrc_audio_sink_ring_buffer_start (AsrcAudioRingBuffer * buf)
{
  AsrcAudioSink *sink;

  sink = ASRC_AUDIO_SINK (GST_OBJECT_PARENT (buf));

  GST_DEBUG_OBJECT (sink, "start, sending signal");
  ASRC_AUDIO_SINK_RING_BUFFER_SIGNAL (buf);

  return TRUE;
}

static gboolean
asrc_audio_sink_ring_buffer_pause (AsrcAudioRingBuffer * buf)
{
  AsrcAudioSink *sink;
  AsrcAudioSinkClass *csink;

  sink = ASRC_AUDIO_SINK (GST_OBJECT_PARENT (buf));
  csink = ASRC_AUDIO_SINK_GET_CLASS (sink);

  /* unblock any pending writes to the audio device */
  if (csink->reset) {
    GST_DEBUG_OBJECT (sink, "reset...");
    csink->reset (sink);
    GST_DEBUG_OBJECT (sink, "reset done");
  }

  return TRUE;
}

static gboolean
asrc_audio_sink_ring_buffer_stop (AsrcAudioRingBuffer * buf)
{
  AsrcAudioSink *sink;
  AsrcAudioSinkClass *csink;

  sink = ASRC_AUDIO_SINK (GST_OBJECT_PARENT (buf));
  csink = ASRC_AUDIO_SINK_GET_CLASS (sink);

  /* unblock any pending writes to the audio device */
  if (csink->reset) {
    GST_DEBUG_OBJECT (sink, "reset...");
    csink->reset (sink);
    GST_DEBUG_OBJECT (sink, "reset done");
  }
#if 0
  if (abuf->running) {
    GST_DEBUG_OBJECT (sink, "stop, waiting...");
    ASRC_AUDIO_SINK_RING_BUFFER_WAIT (buf);
    GST_DEBUG_OBJECT (sink, "stopped");
  }
#endif

  return TRUE;
}

static guint
asrc_audio_sink_ring_buffer_delay (AsrcAudioRingBuffer * buf)
{
  AsrcAudioSink *sink;
  AsrcAudioSinkClass *csink;
  guint res = 0;

  sink = ASRC_AUDIO_SINK (GST_OBJECT_PARENT (buf));
  csink = ASRC_AUDIO_SINK_GET_CLASS (sink);

  if (csink->delay)
    res = csink->delay (sink);

  return res;
}

/* AudioSink signals and args */
enum
{
  /* FILL ME */
  LAST_SIGNAL
};

enum
{
  ARG_0,
};

#define _do_init \
    GST_DEBUG_CATEGORY_INIT (asrc_audio_sink_debug, "audiosink", 0, "audiosink element");
#define asrc_audio_sink_parent_class parent_class
G_DEFINE_TYPE_WITH_CODE (AsrcAudioSink, asrc_audio_sink,
    ASRC_TYPE_AUDIO_BASE_SINK, _do_init);

static AsrcAudioRingBuffer *asrc_audio_sink_create_ringbuffer (AsrcAudioBaseSink *
    sink);

static void
asrc_audio_sink_class_init (AsrcAudioSinkClass * klass)
{
  AsrcAudioBaseSinkClass *asrcaudiobasesink_class;

  asrcaudiobasesink_class = (AsrcAudioBaseSinkClass *) klass;

  asrcaudiobasesink_class->create_ringbuffer =
      GST_DEBUG_FUNCPTR (asrc_audio_sink_create_ringbuffer);

  g_type_class_ref (ASRC_TYPE_AUDIO_SINK_RING_BUFFER);
}

static void
asrc_audio_sink_init (AsrcAudioSink * audiosink)
{
}

static AsrcAudioRingBuffer *
asrc_audio_sink_create_ringbuffer (AsrcAudioBaseSink * sink)
{
  AsrcAudioRingBuffer *buffer;

  GST_DEBUG_OBJECT (sink, "creating ringbuffer");
  buffer = g_object_new (ASRC_TYPE_AUDIO_SINK_RING_BUFFER, NULL);
  GST_DEBUG_OBJECT (sink, "created ringbuffer @%p", buffer);

  return buffer;
}
