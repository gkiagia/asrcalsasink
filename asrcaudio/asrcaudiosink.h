/* GStreamer
 * Copyright (C) 1999,2000 Erik Walthinsen <omega@cse.ogi.edu>
 *                    2005 Wim Taymans <wim@fluendo.com>
 *
 * asrcaudiosink.h:
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#ifndef __ASRC_AUDIO_AUDIO_H__
#include "audio.h"
#endif

#ifndef __ASRC_AUDIO_SINK_H__
#define __ASRC_AUDIO_SINK_H__

#include <gst/gst.h>
#include "asrcaudiobasesink.h"

G_BEGIN_DECLS

#define ASRC_TYPE_AUDIO_SINK             (asrc_audio_sink_get_type())
#define ASRC_AUDIO_SINK(obj)             (G_TYPE_CHECK_INSTANCE_CAST((obj),ASRC_TYPE_AUDIO_SINK,AsrcAudioSink))
#define ASRC_AUDIO_SINK_CLASS(klass)     (G_TYPE_CHECK_CLASS_CAST((klass),ASRC_TYPE_AUDIO_SINK,AsrcAudioSinkClass))
#define ASRC_AUDIO_SINK_GET_CLASS(obj)   (G_TYPE_INSTANCE_GET_CLASS ((obj),ASRC_TYPE_AUDIO_SINK,AsrcAudioSinkClass))
#define GST_IS_AUDIO_SINK(obj)          (G_TYPE_CHECK_INSTANCE_TYPE((obj),ASRC_TYPE_AUDIO_SINK))
#define GST_IS_AUDIO_SINK_CLASS(klass)  (G_TYPE_CHECK_CLASS_TYPE((klass),ASRC_TYPE_AUDIO_SINK))

typedef struct _AsrcAudioSink AsrcAudioSink;
typedef struct _AsrcAudioSinkClass AsrcAudioSinkClass;

/**
 * AsrcAudioSink:
 *
 * Opaque #AsrcAudioSink.
 */
struct _AsrcAudioSink {
  AsrcAudioBaseSink       element;

  /*< private >*/ /* with LOCK */
  GThread   *thread;

  /*< private >*/
  gpointer _gst_reserved[GST_PADDING];
};

/**
 * AsrcAudioSinkClass:
 * @parent_class: the parent class structure.
 * @open: Open the device. No configuration needs to be done at this point.
 *        This function is also used to check if the device is available.
 * @prepare: Prepare the device to operate with the specified parameters.
 * @unprepare: Undo operations done in prepare.
 * @close: Close the device.
 * @write: Write data to the device.
 * @delay: Return how many frames are still in the device. This is used to
 *         drive the synchronisation.
 * @reset: Returns as quickly as possible from a write and flush any pending
 *         samples from the device.
 *
 * #AsrcAudioSink class. Override the vmethods to implement functionality.
 */
struct _AsrcAudioSinkClass {
  AsrcAudioBaseSinkClass parent_class;

  /* vtable */

  /* open the device with given specs */
  gboolean (*open)      (AsrcAudioSink *sink);
  /* prepare resources and state to operate with the given specs */
  gboolean (*prepare)   (AsrcAudioSink *sink, AsrcAudioRingBufferSpec *spec);
  /* undo anything that was done in prepare() */
  gboolean (*unprepare) (AsrcAudioSink *sink);
  /* close the device */
  gboolean (*close)     (AsrcAudioSink *sink);
  /* write samples to the device */
  gint     (*write)     (AsrcAudioSink *sink, gpointer data, guint length);
  /* get number of frames queued in the device */
  guint    (*delay)     (AsrcAudioSink *sink);
  /* reset the audio device, unblock from a write */
  void     (*reset)     (AsrcAudioSink *sink);

  /*< private >*/
  gpointer _gst_reserved[GST_PADDING];
};

GType asrc_audio_sink_get_type(void);

#ifdef G_DEFINE_AUTOPTR_CLEANUP_FUNC
G_DEFINE_AUTOPTR_CLEANUP_FUNC(AsrcAudioSink, gst_object_unref)
#endif

G_END_DECLS

#endif /* __ASRC_AUDIO_SINK_H__ */
