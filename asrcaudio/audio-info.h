/* GStreamer
 * Copyright (C) <1999> Erik Walthinsen <omega@cse.ogi.edu>
 * Library       <2001> Thomas Vander Stichele <thomas@apestaart.org>
 *               <2011> Wim Taymans <wim.taymans@gmail.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#ifndef __ASRC_AUDIO_AUDIO_H__
#include "audio.h"
#endif

#ifndef __ASRC_AUDIO_INFO_H__
#define __ASRC_AUDIO_INFO_H__

G_BEGIN_DECLS

typedef struct _AsrcAudioInfo AsrcAudioInfo;

/**
 * AsrcAudioFlags:
 * @ASRC_AUDIO_FLAG_NONE: no valid flag
 * @ASRC_AUDIO_FLAG_UNPOSITIONED: the position array explicitly
 *     contains unpositioned channels.
 *
 * Extra audio flags
 */
typedef enum {
  ASRC_AUDIO_FLAG_NONE              = 0,
  ASRC_AUDIO_FLAG_UNPOSITIONED      = (1 << 0)
} AsrcAudioFlags;

/**
 * AsrcAudioLayout:
 * @ASRC_AUDIO_LAYOUT_INTERLEAVED: interleaved audio
 * @ASRC_AUDIO_LAYOUT_NON_INTERLEAVED: non-interleaved audio
 *
 * Layout of the audio samples for the different channels.
 */
typedef enum {
  ASRC_AUDIO_LAYOUT_INTERLEAVED = 0,
  ASRC_AUDIO_LAYOUT_NON_INTERLEAVED
} AsrcAudioLayout;

/**
 * AsrcAudioInfo:
 * @finfo: the format info of the audio
 * @flags: additional audio flags
 * @layout: audio layout
 * @rate: the audio sample rate
 * @channels: the number of channels
 * @bpf: the number of bytes for one frame, this is the size of one
 *         sample * @channels
 * @position: the positions for each channel
 *
 * Information describing audio properties. This information can be filled
 * in from GstCaps with asrc_audio_info_from_caps().
 *
 * Use the provided macros to access the info in this structure.
 */
struct _AsrcAudioInfo {
  const AsrcAudioFormatInfo *finfo;
  AsrcAudioFlags             flags;
  AsrcAudioLayout            layout;
  gint                      rate;
  gint                      channels;
  gint                      bpf;
  AsrcAudioChannelPosition   position[64];

  /*< private >*/
  gpointer _gst_reserved[GST_PADDING];
};

#define ASRC_TYPE_AUDIO_INFO                  (asrc_audio_info_get_type ())
GType asrc_audio_info_get_type                (void);

#define ASRC_AUDIO_INFO_IS_VALID(i)           ((i)->finfo != NULL && (i)->rate > 0 && (i)->channels > 0 && (i)->bpf > 0)

#define ASRC_AUDIO_INFO_FORMAT(i)             (ASRC_AUDIO_FORMAT_INFO_FORMAT((i)->finfo))
#define ASRC_AUDIO_INFO_NAME(i)               (ASRC_AUDIO_FORMAT_INFO_NAME((i)->finfo))
#define ASRC_AUDIO_INFO_WIDTH(i)              (ASRC_AUDIO_FORMAT_INFO_WIDTH((i)->finfo))
#define ASRC_AUDIO_INFO_DEPTH(i)              (ASRC_AUDIO_FORMAT_INFO_DEPTH((i)->finfo))
#define ASRC_AUDIO_INFO_BPS(info)             (ASRC_AUDIO_INFO_DEPTH(info) >> 3)

#define ASRC_AUDIO_INFO_IS_INTEGER(i)         (ASRC_AUDIO_FORMAT_INFO_IS_INTEGER((i)->finfo))
#define ASRC_AUDIO_INFO_IS_FLOAT(i)           (ASRC_AUDIO_FORMAT_INFO_IS_FLOAT((i)->finfo))
#define ASRC_AUDIO_INFO_IS_SIGNED(i)          (ASRC_AUDIO_FORMAT_INFO_IS_SIGNED((i)->finfo))

#define ASRC_AUDIO_INFO_ENDIANNESS(i)         (ASRC_AUDIO_FORMAT_INFO_ENDIANNESS((i)->finfo))
#define ASRC_AUDIO_INFO_IS_LITTLE_ENDIAN(i)   (ASRC_AUDIO_FORMAT_INFO_IS_LITTLE_ENDIAN((i)->finfo))
#define ASRC_AUDIO_INFO_IS_BIG_ENDIAN(i)      (ASRC_AUDIO_FORMAT_INFO_IS_BIG_ENDIAN((i)->finfo))

#define ASRC_AUDIO_INFO_FLAGS(info)           ((info)->flags)
#define ASRC_AUDIO_INFO_IS_UNPOSITIONED(info) ((info)->flags & ASRC_AUDIO_FLAG_UNPOSITIONED)
#define ASRC_AUDIO_INFO_LAYOUT(info)          ((info)->layout)

#define ASRC_AUDIO_INFO_RATE(info)            ((info)->rate)
#define ASRC_AUDIO_INFO_CHANNELS(info)        ((info)->channels)
#define ASRC_AUDIO_INFO_BPF(info)             ((info)->bpf)
#define ASRC_AUDIO_INFO_POSITION(info,c)      ((info)->position[c])

AsrcAudioInfo * asrc_audio_info_new         (void);
void           asrc_audio_info_init        (AsrcAudioInfo *info);
AsrcAudioInfo * asrc_audio_info_copy        (const AsrcAudioInfo *info);
void           asrc_audio_info_free        (AsrcAudioInfo *info);

void           asrc_audio_info_set_format  (AsrcAudioInfo *info, AsrcAudioFormat format,
                                           gint rate, gint channels,
                                           const AsrcAudioChannelPosition *position);

gboolean       asrc_audio_info_from_caps   (AsrcAudioInfo *info, const GstCaps *caps);
GstCaps *      asrc_audio_info_to_caps     (const AsrcAudioInfo *info);

gboolean       asrc_audio_info_convert     (const AsrcAudioInfo * info,
                                           GstFormat src_fmt, gint64 src_val,
                                           GstFormat dest_fmt, gint64 * dest_val);

gboolean       asrc_audio_info_is_equal    (const AsrcAudioInfo *info,
                                           const AsrcAudioInfo *other);

#ifdef G_DEFINE_AUTOPTR_CLEANUP_FUNC
G_DEFINE_AUTOPTR_CLEANUP_FUNC(AsrcAudioInfo, asrc_audio_info_free)
#endif

G_END_DECLS

#endif /* __ASRC_AUDIO_INFO_H__ */
