/* GStreamer
 * Copyright (C) <2015> Wim Taymans <wim.taymans@gmail.com>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

#ifndef __ASRC_AUDIO_RESAMPLER_H__
#define __ASRC_AUDIO_RESAMPLER_H__

#include <gst/gst.h>
#include "audio.h"

G_BEGIN_DECLS

typedef struct _AsrcAudioResampler AsrcAudioResampler;

/**
 * ASRC_AUDIO_RESAMPLER_OPT_CUTOFF
 *
 * G_TYPE_DOUBLE, Cutoff parameter for the filter. 0.940 is the default.
 */
#define ASRC_AUDIO_RESAMPLER_OPT_CUTOFF      "AsrcAudioResampler.cutoff"
/**
 * ASRC_AUDIO_RESAMPLER_OPT_STOP_ATTENUTATION
 *
 * G_TYPE_DOUBLE, stopband attenuation in debibels. The attenutation
 * after the stopband for the kaiser window. 85 dB is the default.
 */
#define ASRC_AUDIO_RESAMPLER_OPT_STOP_ATTENUATION "AsrcAudioResampler.stop-attenutation"
/**
 * ASRC_AUDIO_RESAMPLER_OPT_TRANSITION_BANDWIDTH
 *
 * G_TYPE_DOUBLE, transition bandwidth. The width of the
 * transition band for the kaiser window. 0.087 is the default.
 */
#define ASRC_AUDIO_RESAMPLER_OPT_TRANSITION_BANDWIDTH "AsrcAudioResampler.transition-bandwidth"

/**
 * ASRC_AUDIO_RESAMPLER_OPT_CUBIC_B:
 *
 * G_TYPE_DOUBLE, B parameter of the cubic filter.
 * Values between 0.0 and 2.0 are accepted. 1.0 is the default.
 *
 * Below are some values of popular filters:
 *                    B       C
 * Hermite           0.0     0.0
 * Spline            1.0     0.0
 * Catmull-Rom       0.0     1/2
 */
#define ASRC_AUDIO_RESAMPLER_OPT_CUBIC_B      "AsrcAudioResampler.cubic-b"
/**
 * ASRC_AUDIO_RESAMPLER_OPT_CUBIC_C:
 *
 * G_TYPE_DOUBLE, C parameter of the cubic filter.
 * Values between 0.0 and 2.0 are accepted. 0.0 is the default.
 *
 * See #ASRC_AUDIO_RESAMPLER_OPT_CUBIC_B for some more common values
 */
#define ASRC_AUDIO_RESAMPLER_OPT_CUBIC_C      "AsrcAudioResampler.cubic-c"

/**
 * ASRC_AUDIO_RESAMPLER_OPT_N_TAPS:
 *
 * G_TYPE_INT: the number of taps to use for the filter.
 * 0 is the default and selects the taps automatically.
 */
#define ASRC_AUDIO_RESAMPLER_OPT_N_TAPS      "AsrcAudioResampler.n-taps"

/**
 * AsrcAudioResamplerFilterMode:
 * @ASRC_AUDIO_RESAMPLER_FILTER_MODE_INTERPOLATED: Use interpolated filter tables. This
 *     uses less memory but more CPU and is slightly less accurate but it allows for more
 *     efficient variable rate resampling with asrc_audio_resampler_update().
 * @ASRC_AUDIO_RESAMPLER_FILTER_MODE_FULL: Use full filter table. This uses more memory
 *     but less CPU.
 * @ASRC_AUDIO_RESAMPLER_FILTER_MODE_AUTO: Automatically choose between interpolated
 *     and full filter tables.
 *
 * Select for the filter tables should be set up.
 */
typedef enum {
  ASRC_AUDIO_RESAMPLER_FILTER_MODE_INTERPOLATED = (0),
  ASRC_AUDIO_RESAMPLER_FILTER_MODE_FULL,
  ASRC_AUDIO_RESAMPLER_FILTER_MODE_AUTO,
} AsrcAudioResamplerFilterMode;
/**
 * ASRC_AUDIO_RESAMPLER_OPT_FILTER_MODE:
 *
 * ASRC_TYPE_AUDIO_RESAMPLER_FILTER_MODE: how the filter tables should be
 * constructed.
 * ASRC_AUDIO_RESAMPLER_FILTER_MODE_AUTO is the default.
 */
#define ASRC_AUDIO_RESAMPLER_OPT_FILTER_MODE      "AsrcAudioResampler.filter-mode"
/**
 * ASRC_AUDIO_RESAMPLER_OPT_FILTER_MODE_THRESHOLD:
 *
 * G_TYPE_UINT: the amount of memory to use for full filter tables before
 * switching to interpolated filter tables.
 * 1048576 is the default.
 */
#define ASRC_AUDIO_RESAMPLER_OPT_FILTER_MODE_THRESHOLD "AsrcAudioResampler.filter-mode-threshold"

/**
 * AsrcAudioResamplerFilterInterpolation:
 * @ASRC_AUDIO_RESAMPLER_FILTER_INTERPOLATION_NONE: no interpolation
 * @ASRC_AUDIO_RESAMPLER_FILTER_INTERPOLATION_LINEAR: linear interpolation of the
 *   filter coeficients.
 * @ASRC_AUDIO_RESAMPLER_FILTER_INTERPOLATION_CUBIC: cubic interpolation of the
 *   filter coeficients.
 *
 * The different filter interpolation methods.
 */
typedef enum {
  ASRC_AUDIO_RESAMPLER_FILTER_INTERPOLATION_NONE = (0),
  ASRC_AUDIO_RESAMPLER_FILTER_INTERPOLATION_LINEAR,
  ASRC_AUDIO_RESAMPLER_FILTER_INTERPOLATION_CUBIC,
} AsrcAudioResamplerFilterInterpolation;
/**
 * ASRC_AUDIO_RESAMPLER_OPT_FILTER_INTERPOLATION:
 *
 * ASRC_TYPE_AUDIO_RESAMPLER_INTERPOLATION: how the filter coeficients should be
 *    interpolated.
 * ASRC_AUDIO_RESAMPLER_FILTER_INTERPOLATION_CUBIC is default.
 */
#define ASRC_AUDIO_RESAMPLER_OPT_FILTER_INTERPOLATION "AsrcAudioResampler.filter-interpolation"
/**
 * ASRC_AUDIO_RESAMPLER_OPT_FILTER_OVERSAMPLE
 *
 * G_TYPE_UINT, oversampling to use when interpolating filters
 * 8 is the default.
 */
#define ASRC_AUDIO_RESAMPLER_OPT_FILTER_OVERSAMPLE "AsrcAudioResampler.filter-oversample"

/**
 * ASRC_AUDIO_RESAMPLER_OPT_MAX_PHASE_ERROR:
 *
 * G_TYPE_DOUBLE: The maximum allowed phase error when switching sample
 * rates.
 * 0.1 is the default.
 */
#define ASRC_AUDIO_RESAMPLER_OPT_MAX_PHASE_ERROR "AsrcAudioResampler.max-phase-error"

/**
 * AsrcAudioResamplerMethod:
 * @ASRC_AUDIO_RESAMPLER_METHOD_NEAREST: Duplicates the samples when
 *    upsampling and drops when downsampling
 * @ASRC_AUDIO_RESAMPLER_METHOD_LINEAR: Uses linear interpolation to reconstruct
 *    missing samples and averaging to downsample
 * @ASRC_AUDIO_RESAMPLER_METHOD_CUBIC: Uses cubic interpolation
 * @ASRC_AUDIO_RESAMPLER_METHOD_BLACKMAN_NUTTALL: Uses Blackman-Nuttall windowed sinc interpolation
 * @ASRC_AUDIO_RESAMPLER_METHOD_KAISER: Uses Kaiser windowed sinc interpolation
 *
 * Different subsampling and upsampling methods
 *
 * Since: 1.6
 */
typedef enum {
  ASRC_AUDIO_RESAMPLER_METHOD_NEAREST,
  ASRC_AUDIO_RESAMPLER_METHOD_LINEAR,
  ASRC_AUDIO_RESAMPLER_METHOD_CUBIC,
  ASRC_AUDIO_RESAMPLER_METHOD_BLACKMAN_NUTTALL,
  ASRC_AUDIO_RESAMPLER_METHOD_KAISER
} AsrcAudioResamplerMethod;

/**
 * AsrcAudioResamplerFlags:
 * @ASRC_AUDIO_RESAMPLER_FLAG_NONE: no flags
 * @ASRC_AUDIO_RESAMPLER_FLAG_NON_INTERLEAVED_IN: input samples are non-interleaved.
 *    an array of blocks of samples, one for each channel, should be passed to the
 *    resample function.
 * @ASRC_AUDIO_RESAMPLER_FLAG_NON_INTERLEAVED_OUT: output samples are non-interleaved.
 *    an array of blocks of samples, one for each channel, should be passed to the
 *    resample function.
 * @ASRC_AUDIO_RESAMPLER_FLAG_VARIABLE_RATE: optimize for dynamic updates of the sample
 *    rates with asrc_audio_resampler_update(). This will select an interpolating filter
 *    when #ASRC_AUDIO_RESAMPLER_FILTER_MODE_AUTO is configured.
 *
 * Different resampler flags.
 */
typedef enum {
  ASRC_AUDIO_RESAMPLER_FLAG_NONE                 = (0),
  ASRC_AUDIO_RESAMPLER_FLAG_NON_INTERLEAVED_IN   = (1 << 0),
  ASRC_AUDIO_RESAMPLER_FLAG_NON_INTERLEAVED_OUT  = (1 << 1),
  ASRC_AUDIO_RESAMPLER_FLAG_VARIABLE_RATE        = (1 << 2),
} AsrcAudioResamplerFlags;

#define ASRC_AUDIO_RESAMPLER_QUALITY_MIN 0
#define ASRC_AUDIO_RESAMPLER_QUALITY_MAX 10
#define ASRC_AUDIO_RESAMPLER_QUALITY_DEFAULT 4

void           asrc_audio_resampler_options_set_quality   (AsrcAudioResamplerMethod method,
                                                          guint quality,
                                                          gint in_rate, gint out_rate,
                                                          GstStructure *options);

AsrcAudioResampler * asrc_audio_resampler_new              (AsrcAudioResamplerMethod method,
                                                          AsrcAudioResamplerFlags flags,
                                                          AsrcAudioFormat format, gint channels,
                                                          gint in_rate, gint out_rate,
                                                          GstStructure *options);
void                asrc_audio_resampler_free             (AsrcAudioResampler *resampler);

void                asrc_audio_resampler_reset            (AsrcAudioResampler *resampler);

gboolean            asrc_audio_resampler_update           (AsrcAudioResampler *resampler,
                                                          gint in_rate, gint out_rate,
                                                          GstStructure *options);

gsize               asrc_audio_resampler_get_out_frames   (AsrcAudioResampler *resampler,
                                                          gsize in_frames);
gsize               asrc_audio_resampler_get_in_frames    (AsrcAudioResampler *resampler,
                                                          gsize out_frames);

gsize               asrc_audio_resampler_get_max_latency  (AsrcAudioResampler *resampler);

void                asrc_audio_resampler_resample         (AsrcAudioResampler * resampler,
                                                          gpointer in[], gsize in_frames,
                                                          gpointer out[], gsize out_frames);

G_END_DECLS

#endif /* __ASRC_AUDIO_RESAMPLER_H__ */
